<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;



class ExerciseController extends AbstractController {

    /**
     * @Route("/exercise/first", name="exercise_first")
     */
    public function first() {
        return $this->render("exercise/first.html.twig", [
            "number" => random_int(1, 10)
        ]);
    }
    /**
     * @Route("/exercise/second", name="exercise_second")
     */
    public function second (){

        return $this->render("exercise/second.html.twig", [
            "tab" => ["Hello", "Bonjour", "Privet"]
        ]);
    }
}
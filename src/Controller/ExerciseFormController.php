<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Dog;



class ExerciseFormController extends AbstractController
{

    /**
     * @Route("/exercise/exo-form", name="exo_form")
     */
    public function index(Request $request)
    {
        $id = $request->get("id");
        $name = $request->get("name");
        $breed = $request->get("breed");
        $age = $request->get("age");

        $dog = null;
        if ($id && $name && $breed && $age) {
            $dog = new Dog($id, $name, $breed, $age);

            dump($dog);
        }

        return $this->render("exercise/exo-form.html.twig", [
            "dog" => $dog
        ]);
    }
    
}


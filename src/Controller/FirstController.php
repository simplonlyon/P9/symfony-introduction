<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * Les contrôleurs sont une des pierres angulaires des framework MVC
 * Leur but sera de faire l'intermédiaire entre la Vue (l'affichage) et
 * le Modèle (les données à manipuler).
 * Les contrôleurs vont pouvoir donner accès à certaines valeurs à la vue
 * et vont déterminer ce qu'il faudra exécuter lorsque la vue déclenchera
 * certaines actions (exemple: quand je soumet un formulaire, je prend 
 * ses valeurs et je les fais persister en base de données si elles sont
 * valides)
 * En Symfony, il sera préférable qu'un contrôleur hérite de AbstractController
 * ou de Controller qui nous donnerons accès à des méthodes utilitaires
 * à partir du $this
 * On pourra regrouper plusieurs route ayant un lien logiques entres
 * elles au sein d'un même contrôleur.
 */
class FirstController extends AbstractController {

    /**
     * La méthode index est ici liée à une route via le fichier 
     * config/routes.yaml
     * Une méthode/route en Symfony doit renvoyer une instance de la
     * classe Response. Ici, on le fait en utilisant la méthode
     * render qui va créer une instance de Response à partir d'un
     * template twig
     */
    public function index() {
        
        // return new Response("bloup");
        //On indique le fichier template à interpréter et on lui
        //expose une variable "variable" qui aura comme valeur "bloup"
        //cette variable sera utilisable dans le fichier index.html.twig
        return $this->render("index.html.twig", [
            "variable" => "bloup"
        ]);
    }
    //Ici, on définit la route en utilisant l'annotation Route, sans 
    //utiliser le routes.yaml. Il n'y a pas de "meilleure méthode"
    /**
     * @Route("/blip", name="blip")
     */
    public function page() {
        return new Response('<html><head></head></html>');
    }
}
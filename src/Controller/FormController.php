<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class FormController extends AbstractController {

    // On injecte l'objet Request dans notre Route, il permettra
    //de récupérer toutes les informations de la requête HTTP
    //à l'intérieur de la méthode du contrôleur
    /**
     * @Route("/form", name="form")
     */
    public function index(Request $request) { 
        $valeur = $request->get("test");
        
        return $this->render("form.html.twig", [
            "valeur" => $valeur
        ]);
    }
}